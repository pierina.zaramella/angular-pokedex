import { Injectable } from '@angular/core';
import { HelpersService } from './helpers.service';

@Injectable({
  providedIn: 'root'
})
export class PokemonesService {
  /** TODO: This Api Const should be in constants file */
  constructor(private helpers: HelpersService) {}

  getPokemones = async (urlLocationArea) => {
    const { pokemon_encounters } = await this.helpers.getMethod(urlLocationArea)
    return pokemon_encounters 
  }

  getPokemon = (urlPokemon) => {
    return this.helpers.getMethod(urlPokemon)
  }
}
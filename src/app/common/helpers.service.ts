import { Injectable } from '@angular/core';
import axios from 'axios';

@Injectable({
  providedIn: 'root'
})
export class HelpersService {

  constructor() { }

  getMethod = async (url, message="") => {
    const {data, status} = await axios.get( url )
    
  if(status === 200) {
      return data
    } 
    throw new Error (message ? message : "Error getting data")
  }
}

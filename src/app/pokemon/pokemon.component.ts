import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { PokemonesService } from '../common/pokemones.service'

@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon.component.html',
  styleUrls: ['./pokemon.component.scss']
})
export class PokemonComponent implements OnInit {
  pokemon: {}
  url = ''
  constructor(private route: ActivatedRoute, private pokemonesService: PokemonesService) { }

  ngOnInit() {
    this.route.paramMap.subscribe(async params => {
      this.url = params.get('pokemonUrl');
      if(this.url){
        this.pokemon = await this.pokemonesService.getPokemon(this.url)
      }
    });
  }

}

import { Component, OnInit } from '@angular/core';

import { AreasService } from '../common/areas.service'

@Component({
  selector: 'app-areas',
  templateUrl: './areas.component.html',
  styleUrls: ['./areas.component.scss']
})
export class AreasComponent implements OnInit {
  areas = []
  locationArea = ''
  showErrorMessage = false;

  constructor(private areaService: AreasService) { }

  ngOnInit() {
    this.getAreas()
  }

  getAreas = async () => {
    try {
      this.areas = await this.areaService.getAreas()
      console.log(this.areas)
    } catch (e) {
      this.showErrorMessage = true
    } 
  }

  areaSelected = async (event) => {
    console.log("event", event)
    const location = await this.areaService.getLocations(event.value)
    this.locationArea = location[0].url
  }
}

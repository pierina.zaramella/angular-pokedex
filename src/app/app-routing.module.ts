import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PokemonComponent } from './pokemon/pokemon.component';
import { AreasComponent } from './areas/areas.component';


const routes: Routes = [
  {path: '', component: AreasComponent},
  {path: 'pokemon/:pokemonUrl', component: PokemonComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

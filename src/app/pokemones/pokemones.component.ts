import { Component, OnInit, Input } from '@angular/core';

import { PokemonesService } from '../common/pokemones.service'
import { Sort } from '@angular/material/sort';

@Component({
  selector: 'app-pokemones',
  templateUrl: './pokemones.component.html',
  styleUrls: ['./pokemones.component.scss']
})
export class PokemonesComponent implements OnInit {
  @Input() urlAreasLocation = ''
  pokemones = []
  pokemon = {}
  displayedColumns: string[] = [ 'name'];
  sortedData = []

  constructor(private pokemonesService: PokemonesService) { }

  ngOnInit() {
    this.getPokemones(this.urlAreasLocation)
  }

  getPokemones = async (urlAreasLocation) => {
      this.pokemones = await this.pokemonesService.getPokemones(urlAreasLocation) 
      this.sortedData = this.pokemones.slice()
      console.log('this.pokemones ', this.pokemones )
  }

  sortData = (sort: Sort) => {
    const data = this.pokemones.slice()
    if (!sort.active || sort.direction === '') {
      this.sortedData = this.pokemones;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'name': return this.compare(a.pokemon.name, b.pokemon.name, isAsc);
        default: return 0;
      }
    });
  }

  compare = (a: number | string, b: number | string, isAsc: boolean) => {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  applyFilter = (str: string) => {
    str = str.toLocaleLowerCase()
    this.sortedData = this.pokemones.filter(item => item.pokemon.name.indexOf(str) > -1)
  }

  getPokemon = async (urlPokemon) => {
    this.pokemon = await this.pokemonesService.getPokemon(urlPokemon) 
  }
}

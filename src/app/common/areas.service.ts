import { Injectable } from '@angular/core';
import { HelpersService } from './helpers.service';

@Injectable({
  providedIn: 'root'
})
export class AreasService {
/** TODO: This Api Const should be in constants file */
  api = { urlLocations: 'https://pokeapi.co/api/v2/location'  }

  constructor(private helpers: HelpersService) { }

  getAreas = async () => {
    const { results } = await this.helpers.getMethod(this.api.urlLocations)
    return results
  }

  getLocations = async (urlLocations) => {
    const { areas } = await this.helpers.getMethod(urlLocations)
    return areas
 }
}